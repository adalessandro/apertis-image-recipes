{{- $architecture := "armhf" }}
{{- $type := "minimal" }}
{{- $mirror := "https://repositories.apertis.org/apertis/" }}
{{- $suite := "v2022dev1" }}
{{- $timestamp := "" }}
{{- $image := (printf "sample-production-%s-%s-%s" $suite  $type $architecture) }}
{{- $osname := "mydistribution" }}

{{ $board := "uboot" }}
{{ $branch := (printf "%s/%s/%s-%s/%s" $osname $suite $architecture $board $type) }}
{{ $ostree := "repo" }}
{{ $repourl := (printf "${ARTIFACTDIR}/%s" $ostree) }}

architecture: {{ $architecture }}

actions:
  - action: run
    description: Create local OSTree repository
    chroot: false
    command: ostree init --mode archive --repo=${ARTIFACTDIR}/{{ $ostree }}

  # Create base rootfs
  - action: recipe
    description: Create ospack for {{ $suite }} {{ $type }} {{ $architecture }}
    recipe: ospack-{{ $type }}.yaml
    variables:
      osname: {{ $osname }}
      keyring: apertis-archive-keyring
      type: {{ $type }}
      mirror: {{ $mirror }}
      suite: {{ $suite }}
      timestamp: {{ $timestamp }}
      pack: false

  # Run scripts to customize your rootfs
  - action: run
    description: "Customize image version metadata"
    chroot: true
    script: scripts/setup_image_version.sh {{ $osname }} {{ $suite }} '{{ $timestamp }}' mycompany {{ $type }}

  # Customize your package selection
  - action: apt
    description: Install Python
    packages:
      - python3

  # Adapt and commit OSTree based on rootfs created during previous action
  - action: recipe
    description: OSTree commit for {{ $suite }} {{ $type }} {{ $architecture }}
    recipe: ostree-commit.yaml
    variables:
      osname: {{ $osname }}
      keyring: apertis-archive-keyring
      type: {{ $type }}
      suite: {{ $suite }}
      timestamp: {{ $timestamp }}
      board: {{ $board }}
      branch: {{ $branch }}
      image: {{ $image }}
      ostree: {{ $ostree }}
      unpack: false

  # Reset the rootfs to allow to deploy OSTree from a clean rootfs
  - action: run
    description: Reset rootfs before deploying OSTree
    chroot: false
    command: find ${ROOTDIR} -maxdepth 1 -mindepth 1 -exec rm -rf {} \;

  # Package up everything in a image
  - action: recipe
    description: Create image {{ $image }} for {{ $suite }} {{ $type }} {{ $architecture }}
    recipe: ostree-image-uboot.yaml
    variables:
      osname: {{ $osname }}
      keyring: apertis-archive-keyring
      type: {{ $type }}
      suite: {{ $suite }}
      image: {{ $image }}
      board: {{ $board }}
      repourl: {{ $repourl }}
      branch: {{ $branch }}
      ostree: {{ $ostree }}
