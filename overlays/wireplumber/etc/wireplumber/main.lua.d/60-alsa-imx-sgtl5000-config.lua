table.insert(alsa_monitor.rules, {
  matches = {
    {
      { "alsa.driver_name", "equals", "snd_soc_imx_sgtl5000" }
    }
  },
  apply_properties = {
    ["audio.format"] = "S16LE",
  }
})
