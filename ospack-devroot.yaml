{{- $architecture := or .architecture "amd64" }}
{{- $type := or .type "devroot" -}}
{{- $mirror := or .mirror "https://repositories.apertis.org/apertis/" -}}
{{- $suite := or .suite "v2022dev3" -}}
{{- $timestamp := or .timestamp "" -}}
{{- $snapshot := or .snapshot "" -}}
{{- $ospack := or .ospack (printf "ospack_%s-%s-%s" $suite $architecture $type) -}}
{{- $stable := or .stable "" -}}
{{- $osname := or .osname "apertis" -}}
{{- $keyring := or .keyring (printf "%s-archive-keyring" $osname) -}}

architecture: {{ $architecture }}

actions:
  - action: debootstrap
    suite: {{if eq $snapshot ""}} {{ $suite }} {{else}} {{ $suite }}/snapshots/{{ $snapshot }} {{end}}
    components:
      - target
    mirror: {{ $mirror }}
    variant: minbase
    keyring-package: {{ $keyring }}
    keyring-file: keyring/{{ $keyring }}.gpg
    merged-usr: false

  - action: overlay
    source: overlays/locale-default-c-utf8

  - action: overlay
    description: Work around "Hash Sum Mismatch" errors, https://phabricator.collabora.com/T15071
    source: overlays/apt-disable-http-pipelining

  # Add image version information
  - action: run
    description: "Setting up image version metadata"
    chroot: true
    script: scripts/setup_image_version.sh {{ $osname }} {{ $suite }} '{{ $timestamp }}' collabora {{ $type }}

  - action: run
    description: "Add extra apt sources"
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} --sources {{if eq $stable "true"}} --updates --security {{end}} target hmi development sdk {{if ne $snapshot ""}} --snapshot {{ $snapshot }} {{end}}

  - action: run
    description: "Switch to the latest coreutils package (GPLv3)"
    chroot: true
    script: scripts/replace-gplv2-packages-for-dev-env.sh

  - action: apt
    description: "Switch to gpgv package (GPLv3)"
    packages:
      - gpgv

  - action: apt
    description: "Core packages"
    packages:
      - apt
      - apt-transport-https
      - busybox
      - ca-certificates
      - libnss-myhostname
      - netbase
      - sudo

  - action: apt
    description: "Basic tooling packages"
    packages:
      - apt-utils
      - bash
      - bash-completion
      - bzip2
      - file
      - gnupg
      - iproute2
      - iputils-ping
      - less
      - lzma
      - mawk
      - vim-tiny
      - wget
      - whiptail

  # the goal here is to ship enough dependencies to build very small
  # packages like netkit-telnet
  - action: apt
    description: "Development packages"
    packages:
      - autoconf-archive
      - automake
      - autopoint
      - autotools-dev
      - bison
      - build-essential
      - chrpath
      - cmake
      - cmake-data
      - debhelper
      - devscripts
      - fakeroot
      - flex
      - gawk
      - gcc
      - git
      - gperf
      - libtool
      - lsb-release
      - meson
      - pkg-config
      - symlinks
      - zip

  - action: run
    description: Set the hostname
    chroot: false
    command: echo "{{ $osname }}" > "$ROOTDIR/etc/hostname"

  - action: overlay
    source: overlays/default-hosts

  - action: overlay
    source: overlays/loopback-interface

  - action: overlay
    source: overlays/create-homedir

  - action: overlay
    source: overlays/sudo-fqdn

  - action: overlay
    source: overlays/fsck

  - action: run
    chroot: true
    script: scripts/add-xdg-user-metadata.sh

  - action: run
    chroot: true
    script: scripts/create-mtab-symlink.hook.sh

  - action: run
    chroot: true
    script: scripts/setup_user.sh

  - action: run
    chroot: true
    script: scripts/add_user_to_groups.sh

  - action: run
    chroot: true
    script: scripts/check_sudoers_for_admin.sh

  - action: run
    description: Switch to live APT repos
    chroot: true
    script: scripts/switch-apt-to-live.sh -r {{ $suite }}

  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $ospack }}.pkglist.gz"

  - action: run
    description: Cleanup /var/lib
    script: scripts/remove_var_lib_parts.sh

  - action: run
    description: List files on {{ $ospack }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $ospack }}.filelist.gz"

  - action: pack
    compression: gz
    file: {{ $ospack }}.tar.gz
