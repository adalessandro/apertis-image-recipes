#!/bin/bash

set -e

if [ -z  "${ROOTDIR}" ] ; then
  echo "ROOTDIR not given"
  exit 1
fi

osname=$1

bootconf=${ROOTDIR}/boot/loader/entries/ostree-1-${osname}.conf

if [ ! -f "$bootconf" ]; then
    echo "OStree setup is not available!"
    exit 1
fi

ostree=$(grep -o 'ostree=[/.[:alnum:]]\+' $bootconf)
ostree="${ROOTDIR}${ostree#*=}"

mkdir -p ${ROOTDIR}/boot/firmware
cp -av ${ostree}/usr/lib/raspi-firmware/* ${ROOTDIR}/boot/firmware/

# Copy U-Boot
cp -av ${ostree}/usr/lib/u-boot/rpi_arm64/u-boot.bin ${ROOTDIR}/boot/firmware/

# Copy DTBs from the kernel
cp -av ${ostree}/usr/lib/ostree-boot/dtbs-*-arm64*/broadcom/* ${ROOTDIR}/boot/firmware/
