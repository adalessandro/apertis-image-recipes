#!/bin/sh

set -e

build_dir=$(mktemp -d)
firmware_dir=$ROOTDIR/deb-binaries/usr/lib/firmware/imx8mn/ddr/synopsys
uboot_dir=$ROOTDIR/deb-binaries/usr/lib/u-boot/imx8mn_var_som

usage() {
	echo "$0 <flash.bin>
	Build flash.bin u-boot binary image.

	Packages u-boot and imx-firmware-ddr4 are expected to be
	installed by chdist.

	options:
	 -h           Display this help and exit
	"
	exit 1
}

gen_mkimage_cfg() {
	# Generate mkimage configuration file.
	# Based on u-boot arch/arm/mach-imx/imx8m/imximage-8mn-ddr4.cfg
	cat << EOF
FIT
ROM_VERSION v2
BOOT_FROM sd
LOADER u-boot-spl-ddr.bin 0x912000
SECOND_LOADER u-boot.itb 0x40200000 0x60000
EOF
}

gen_boot_spl_ddr() {
	# Generate u-boot-spl-ddr.bin file.
	# Based on u-boot tools/imx8m_image.sh
	objcopy -I binary -O binary --pad-to 0x8000 --gap-fill=0x0 ddr4_imem_1d_201810.bin ddr4_imem_1d_pad.bin
	objcopy -I binary -O binary --pad-to 0x4000 --gap-fill=0x0 ddr4_dmem_1d_201810.bin ddr4_dmem_1d_pad.bin
	objcopy -I binary -O binary --pad-to 0x8000 --gap-fill=0x0 ddr4_imem_2d_201810.bin ddr4_imem_2d_pad.bin
	cat ddr4_imem_1d_pad.bin ddr4_dmem_1d_pad.bin > ddr4_1d_fw.bin
	cat ddr4_imem_2d_pad.bin ddr4_dmem_2d_201810.bin > ddr4_2d_fw.bin
	dd if=u-boot-spl.bin of=u-boot-spl-pad.bin bs=4 conv=sync
	cat u-boot-spl-pad.bin ddr4_1d_fw.bin ddr4_2d_fw.bin > u-boot-spl-ddr.bin
	rm -f ddr4_1d_fw.bin ddr4_2d_fw.bin ddr4_imem_1d_pad.bin ddr4_dmem_1d_pad.bin ddr4_imem_2d_pad.bin u-boot-spl-pad.bin
}


if [ $# -ne 1 ]; then
	echo Error: Need output image filename. >&2
	echo >&2
	usage
	exit 1
fi

output_image=$1

mkdir -p $build_dir/

cp $firmware_dir/ddr4_*.bin $build_dir/
cp $uboot_dir/u-boot-spl.bin $build_dir/
cp $uboot_dir/u-boot.itb $build_dir/

cd $build_dir

gen_boot_spl_ddr
gen_mkimage_cfg > imximage-8mn-ddr4.cfg
mkimage -n imximage-8mn-ddr4.cfg -T imx8mimage -e 0x912000 -d u-boot-spl-ddr.bin $output_image
