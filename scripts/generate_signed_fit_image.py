#!/usr/bin/python3

import sys
import os
import argparse
import tempfile
import subprocess

from jinja2 import Environment, FileSystemLoader

# Default address for our U-Boot on SabreLite
FIT_LOADADDR=0x12000000
FIT_PAD=0x1000

e = Environment(loader=FileSystemLoader(''))

# Return the size of padded image with IVT
def add_ivt(image):
    offset = os.stat(image).st_size
    offset = (offset + FIT_PAD - 1) & ~(FIT_PAD - 1)

    address = FIT_LOADADDR + offset

    with open(image, 'ab') as f:
        f.truncate(offset)

        f.write(b'\xD1') # IVT
        f.write(b'\x00\x20') # 0x20 bytes long
        f.write(b'\x41') # Hab v4

        f.write(FIT_LOADADDR.to_bytes(4, byteorder='little'))
        f.write((0).to_bytes(4, byteorder='little')) # reserved
        f.write((0).to_bytes(4, byteorder='little')) # DCD)
        f.write((0).to_bytes(4, byteorder='little')) #boot data
        f.write((address).to_bytes(4, byteorder='little')) # self pointer
        f.write((address + 0x20).to_bytes(4, byteorder='little')) # csf pointer
        f.write((0).to_bytes(4, byteorder='little')) # reserved 2
        f.flush()

    print("IVT offset = ", hex(offset))

    return (offset + 0x20)

def fit_image_create(kernel, ramdisk, dtb, fit_template, fitimage):
    # Need absolute or relative path to binaries for mkimage
    kernel = os.path.realpath(kernel)
    ramdisk = os.path.realpath(ramdisk)
    dtb = os.path.realpath (dtb)

    template = e.get_template(fit_template)
    with tempfile.NamedTemporaryFile(prefix="fitimage_", suffix=".its", mode="w") as ivt:
        config = template.render(kernel=kernel, ramdisk=ramdisk, dtb=dtb)
        print("Generated configuration for FIT image:")
        print(config)
        ivt.write(config)
        ivt.flush()

        print("Generating FIT image:")
        subprocess.run(["mkimage", "-f", ivt.name, fitimage], check=True)

# Sign the whole image with assumption it is loaded at FIT_LOADADDR
def sign_image(image, csf_template, length):
    signfile = "%s.bin" % image
    template = e.get_template(csf_template)
    with tempfile.NamedTemporaryFile(prefix="fitimage_", suffix=".csf", mode="w") as csf:
        config = template.render(loadaddr=hex(FIT_LOADADDR), offset="0x00000000",
                                 length=hex(length), image=image)
        print("Generated CSF for signing:")
        print(config)
        csf.write(config)
        csf.flush()

        # Signing
        subprocess.run (["cst", "-i", csf.name, "-o", signfile ], check=True)

    # Inject csf ; assume f is at the end of ivt
    with open(image, 'ab') as f:
        c = open(signfile, "rb");
        f.write(c.read())
        f.flush()
        c.close()

    print("Image \"%s\" signed successfully" % image)


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument ("-k","--kernel", required=True, help="path to kernel")
    parser.add_argument ("-r","--ramdisk", required=True, help="path to ramdisk image")
    parser.add_argument ("-d","--dtb", required=True, help="path to DTB file")
    parser.add_argument ("-t","--itstemplate", required=True, help="ITS template for FIT image creation")
    parser.add_argument ("-s","--csftemplate", required=True, help="CSF template for FIT image signing")
    parser.add_argument ("-o","--out", required=True, help="signed FIT image name")
    args = parser.parse_args ()

    fit_image_create (args.kernel, args.ramdisk, args.dtb, args.itstemplate, args.out)
    fit_size = add_ivt (args.out)
    sign_image (args.out, args.csftemplate, fit_size)

if __name__ == "__main__" :
    main(sys.argv[1:])
