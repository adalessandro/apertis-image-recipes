Apertis Image Recipes
=====================

This repository contains the recipes used to build the Apertis reference images.

Getting started
===============

To experiment with Apertis, begin with the `sample-*.yaml` recipes.
They are a good starting point for building your own images based on the
reference Apertis recipes.

From the Apertis SDK, build your first image:

    $ sudo debos sample-image-development.yaml

The `sample-image-development.yaml` recipe builds an image suitable for
the ARM-based i.MX6 SABRE Lite board with all the development tools installed.

Copy it to a SD card and then plug the SD card in your board to boot Apertis
on it.

The image can be copied to the SD card by decompressing it and then copying the
raw bytes using tools like `dd`.

If the SD Card reader is available from the Apertis SDK, the
[`bmaptool`](https://github.com/intel/bmap-tools) command provides an
alternative that is faster, more reliable and easy to use:

    $ sudo bmaptool copy sample-development-*-minimal-armhf.img.gz $SDCARD

You can now connect to your board via a serial connection, the default
credentials are `user:user`.

Depending on your network setup, your board may also be available via SSH with
the same credentials:

    $ ssh user@apertis.local

With the development image you can install, remove, and update packages using
the standard Debian tools like `apt`, including the GPLv3 components that are
excluded by default from the Apertis image:

    $ sudo apt install gdbserver

You can edit the recipe to customize the package selection, run custom scripts
to change the rootfs contents, and download contents from remote sources.

For images to be deployed in production, Apertis uses OSTree to manage updates
in a reliable way.

The `sample-image-production.yaml` recipe builds an image that boots
into an OSTree deployment with the ability to automatically roll-back updates
in case of boot failures:

    $ sudo debos sample-image-production.yaml
    $ sudo bmaptool copy sample-production-*-minimal-armhf.img.gz $SDCARD

Providing a reproducible development environment is one of the key elements for
Apertis, and the `sample-image-sdk.yaml` gives you the ability to
generate your own SDK images, bootable in VirtualBox:

    $ sudo debos --scratchsize 10G sample-image-sdk.yaml

You can then pick the generated VDI file and
[set it up in VirtualBox](https://wiki.apertis.org/VirtualBox).

Customizing the recipes
=======================

Open the sample recipes and add, remove or edit their actions to make the
generated artifacts suit your needs.

Some customization examples are already provided, check the Debos documentation
for a full list of the available actions:

https://godoc.org/github.com/go-debos/debos/actions

The reference recipes
=====================

The main `.gitlab-ci.yml` orchestrates how platform-independent ospacks and
platform-specific hwpacks are combined to generate all the reference artifacts.

Architectures supported via `-t architecture:$VALUE`:
  * arm64: (uboot) currently targets ARM 64bit Renesas R-Car boards
  * amd64: (uefi) supports any UEFI x86-64bit system
  * armhf: (uboot) currently targets the ARM 32bit i.MX6 SABRE Lite

Package set selection via `-t type:$VALUE`:
  * minimal: headless system
  * target: Mildenhall HMI
  * sdk: XFCE-based development environment with the Mildenhall HMI tools
  * basesdk: basic XFCE-based development environment
  * sysroot: cross-compilation target tarballs
  * devroot: emulation-based foreign platform build environments

Deployment types:
  * APT: suitable for development, prioritizes flexibility over robustness
  * OSTree: suitable for production, prioritizes robustness over flexibility

Firmware installer images:
  * u-boot automatic installer image for i.MX6 SABRE Lite

Parameters for ospack and image versioning:
  * suite: `-t suite:v2021dev3`
  * timestamp: `-t timestamp:$(date +%s)`

Building in Docker
==================

To build in a reproducible environment outside of the Apertis SDK, using the
pre-built Docker images in the Apertis registry is recommended:

    $ RELEASE=v2021dev3 # the Apertis release to be built
    $ docker run \
      -i -t --rm \
      -w /recipes \
      -v $(pwd):/recipes \
      --security-opt label=disable \
      -u $(id -u):$(id -g) \
      --group-add=$(getent group kvm | cut -d : -f 3) \
      --device /dev/kvm \
      --cap-add=SYS_PTRACE \
      --tmpfs /scratch:exec \
      registry.gitlab.apertis.org/infrastructure/apertis-docker-images/$RELEASE-image-builder \
      debos ospack-minimal.yaml -t suite:$RELEASE

The image build will be run in the Docker container using your uid.
By granting access to the KVM device where hardware virtualization is available
Debos/Fakemachine can automatically select their KVM backend which provides
better performance.
Otherwise the ptrace and scratch tmpfs bits enable the use of the User Mode Linux
backend, which works where hardware virtualization is not available, which is
often the case on virtual machines in the cloud.

GitLab CI/CD automation
=======================

The reference images are built daily and for each commit with a rather big
GitLab CI/CD pipeline.

The jobs in the pipeline make heavy usage of the `image-builder` Docker
container image from the infrastructure/apertis-docker-images> project.

The [`.gitlab-ci.yml`](gitlab-ci.yml) file defines all the jobs in the
pipeline by instantiating the templates included from the
[`.gitlab-ci/`](.gitlab-ci/) folder:

* `templates-ospack-build.yml`: building the hardware-agnostic portion of the
  rootfs (called `ospack`) for each architecture and image variant is the
  initial step of the build process, and this file provides the template
  for building them;
* `templates-ostree-commit.yml`: this file provides the template to commit an
  ospack into an OSTree repository;
* `templates-artifacts-build.yml`: all the templates producing the actual
  images and bundles for offline updates are here;
* `templates-upload.yml`: contains the templates for uploading the artifacts to
  the storage server;
* `templates-submit-tests.yml`: ships the template to submit test jobs to LAVA
  exercising the artifacts on actual hardware.

A current requirement for the upload jobs is the availability of `ostree-push`
on the storage server to efficiently upload OSTree commits, while other
artifacts are uploaded with `rsync` over SSH.

The GitLab runners for the build jobs need to be set up accordingly to the
[Building in Docker](#building-in-docker) section to be able to run the
UML backend for Debos:

* `--docker-cap-add=SYS_PTRACE`
* `--docker-tmpfs='/scratch:rw,exec'`

GitLab CI/CD variables
======================

OSTREE_SIGNING_KEY
------------------
OSTree commits and static delta bundles can be signed using **ed25519** key.

### Generating keys

Following commands can be used to generate **ed25519** keys:
```
  # Generate ed25519 certificate
$ openssl genpkey -algorithm ed25519 -outform PEM -out /tmp/key.pem

  # Extract the private and public parts from generated key.
  # Based on: http://openssl.6102.n7.nabble.com/ed25519-key-generation-td73907.html
$ ED25519PUBLIC="$(openssl pkey -outform DER -pubout -in ${pemfile} | tail -c 32 | base64)"
$ ED25519SEED="$(openssl pkey -outform DER -in ${pemfile} | tail -c 32 | base64)"
  # Secret key is concantination of SEED and PUBLIC
$ ED25519SECRET="$(echo ${ED25519SEED}${ED25519PUBLIC} | base64 -d | base64 -w 0)"

$ echo "ED25519PUBLIC = ${ED25519PUBLIC}"
$ echo "ED25519SECRET = ${ED25519SECRET}"
```

### Providing secret key to GitLab CI/CD

The **secret key** should be provided to GitLab CI/CD in *Settings CI/CD* Variables with:

| Type | Key                | Value              | Protected | Masked | Environments |
| ---- | ------------------ | ------------------ | --------- | ------ | ------------ |
| File | OSTREE_SIGNING_KEY | ed25519 secret key | x         | x      | All          |

### Providing public key to Apertis

The **public key** used to verify static delta bundle in Apertis should be copied to `overlays/ed25519/usr/share/ostree/trusted.ed25519.d/apertis.ed25519`

